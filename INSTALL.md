# Installing KMD #

1. [Requirements](#requirements)
   1. [General](#general-requirements)
   2. [Windows](#requirements-for-windows)
2. [Installation](#installation)
   1. [Windows](#windows)
   2. [Linux](#linux)
      1. [Terminal](#terminal)
      2. [XKB](#install_linux_x)


## Requirements ##

### General Requirements ###
- A 105-key keyboard! Many keyboards (particuarly those made for US-QWERTY) only
  have 104 keys and are thus missing the key that this layout uses for
  <kbd>/</kbd>, <kbd>\\</kbd>, and <kbd>|</kbd>.\
  If you don't know what that means there is a [quite useful picture][keyboard comparison]
  on Wikipedia Commons.

### Requirements for Windows ###
- [MSKLC][MSKLC "Microsoft Keyboard Layout Creator"]


## Installation ##

### Windows ###
- Either **clone this project** or **download [`KMD.klc`](Windows/KMD.klc)**
- Open **MSKLC** and set the **current working directory** to the location
   of your `KMD.klc`
- Select `File > Load Source File...` and select `KMD.klc` through the
   window that should open
- Select `Project > Build DLL and Setup Package`\
   This will create a file contains a *bunch* of Warnigs... You won't have
   to worry about those
- Open the created `kmd` folder and run `setup.exe`\
   **Do not delete** this folder. It is very usefull for uninstalling the
   layout, which you'll have to do if you want to update it.

### Linux ###
Linux has various layout formats depending on how you're using it. You probably
want to follow the instructions for Xorg/XKB.

#### Terminal ####
* Either **clone this project** or **download [`kmd.map`](Linux/kmd.map)**
* Compress kmd.map using gzip
* Copy `kmd.map.gzip` into `/usr/share/kbd/keymaps/`
* To use the layout **temporarily** do
```bash
loadkeys kmd
```
* To use it **permanently** write into `/etc/vconsole.conf` `KEYMAP=kmd`

#### Xorg ####
This requries some understanding of xml and root privileges. You might also have
to repeat these instructions when Xorg updates change the relevant files.

X sorts  its keyboard layouts primarily by country using two-letter country
codes. You can add this layout to any of these lists. For this you will need to
do two things (I *think*, at least doing these works for me):

1. Append the layout to your country's list of layouts.
   - After **cloning this project** or **downloading** [`kmd.xkb`](Linux/kmd.xkb)
   - Pick a file in `/usr/share/X11/xkb/symbols/` and simply **append** the
     contents of `kmd.xkb`
2. Add the layout to `/usr/share/X11/xkb/rules/base.extras.xml/`
   - Find the `layout` with the name of the file to which you added the layout
     in step 1.
   - Add the following to its `variantlist`

```
<variant>
  <configItem popularity="exotic">
    <name>kmd</name>
    <description>Multilingual (Katrin Multilingual Dvorak)</description>
  </configItem>
</variant>
```

To use the layout for the session do

```sh
setxkbmap -layout YOUR_COUNTRY -variant kmd
```

- If you want to set it **permanently** see [the arch wkik][Arch XKB config]
  (even if you don't use arch — X is still X)


[Arch XKB config]: https://wiki.archlinux.org/title/Xorg/Keyboard_configuration
[keyboard comparison]: https://en.wikipedia.org/wiki/File:Physical_keyboard_layouts_comparison_ANSI_ISO_KS_ABNT_JIS.png
[MSKLC]: https://www.microsoft.com/en-us/download/details.aspx?id=102134
