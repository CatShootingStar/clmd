# TODO #
1. [General](#general)
2. [Implementation](#implementation)
   1. [Linux](#linux)
   2. [Windows](#windows)
   3. [MacOS](#macos)
3. [Proposals](#proposals)


## General ##
- [ ] Pretty sure I'm missing some Greek characters
- [ ] There are various letters in CMS that I'm missing
  - [ ] ŋ, Ŋ, ĳ, Ĳ, ŀ, Ŀ
  - [ ] double acute accent
- [x] Add INSTALL.md
- [ ] Make GitLab release (once all above points have been dealt with for¨
      Windows and linux)


## Implementation ##
- [x] There seem to be varrious differences between the linux and windows
      layouts

### Linux ###
- [x] Console layout
  - [x] altgr keycode 26
  - [x] Add comments
- [x] XKB layout
  - As always, the arch repo has [an explanation][Arch XKB] for it
  - A [simple Introduction][XKB guide] to the structure of XKB
  - Many relevant xkb files are in `/usr/share/X11/xkb/`. The freedesktop
    foundation provides a vague [guide][XKB config rules] on editing these
  - [x] Make `xkb_symbols` standalone
    - [x] Add instructions for installation
  - [x] Clean up `kmd.xkb`
    - [x] Add comments
- What more could there be?

### Windows ###
- [ ] Chained deadkeys (to write characters such as Ḝ or the complex Greek
      combinations)

### MacOS ###
- [ ] Layout


## Proposals ##
* [ ] A seperate ANSI version, which doesn't rely on scancode 0x56
* [ ] Cyrillic support
* [ ] Other paranthesis such as 【 】
* [ ] Shove `Control_asciicircum` and `Meta_asciicircum` somewhere into the
      console keymap (linux)
* [ ] Other hyphens, such as em-dash and en-dash
* [ ] Make *ring above*, *space* print a degree sign


[XKB guide]: https://medium.com/@damko/a-simple-but-comprehensive-guide-to-xkb-for-linux-6f1ad5e13450
[Arch XKB]: https://wiki.archlinux.org/title/X_keyboard_extension
[XKB config rules]: https://www.freedesktop.org/wiki/Software/XKeyboardConfig/Rules/

