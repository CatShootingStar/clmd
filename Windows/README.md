# Windows layout #
1. [The `.klc` file](#the_klc_file)\
   1.1 [File format](#file_format)\
   1.2 [File structure](#file_structure)\
   1.3 [Shiftstates](#shiftstates)\
   1.4 [Ligature](#ligature)
2. [If anyone wants to rewrite this in C](#c_rewrite)


<a name="the_klc_file"></a>
# The `.klc` File #
The *Microsoft Keyboard Layout Creator* (henceforth MSKLC) - Microsoft's
official software for creating custom keyboard layouts - doesn't appear to be
able to do everything that Windows' build-in layouts are able to do.
Particularly combinations such as <kbd>r-ctrl</kbd> + <kbd>key</kbd> don't
appear to be doable. There are, however, some advanced things you can do by
editing the `.klc` file in plain text rathern than through the MSKLC software.
These include stringed deadkeys and remapping of virtual keys.

---

*Everything I write here is just what I think is happening.*

<a name="file_format"></a>
## File format ##

### Required: ###
* Encoding: `UTF-16 LE`
* EOL character: `CRLF`

### Recommended: ###
* Use tabs rather than spaces
* Tab width: 8

Comments are preceded by either `//` or `;`

<a name="file_structure"></a>
## File structure ##

```
KBD	...

COPYRIGHT	...

COMPANY		...

LOCALENAME	...

LOCALEID	...

VERSION		...

ATTRIBUTES
...

SHIFTSTATE

...

LAYOUT

...

LIGATURE

...

DEADKEY ...

...

KEYNAME

...

KEYNAME_EXT

...

KEYNAME_DEAD

...

DESCRIPTION

...

LANGUAGENAMES

...

ENDKBD
```

<a name="shiftstates"></a>
## Shiftstates ##

Order does not strictly matter, but it will affect the order of the rows.

0  -> nada\
1  -> <kbd>Shift</kbd>\
2  -> <kbd>Control</kbd>\
3  -> <kbd>Shift</kbd> + <kbd>Control</kbd>\
6  -> <kbd>Control</kbd> + <kbd>Alt</kbd> aka. <kbd>Alt Gr</kbd>\
7  -> <kbd>Shift</kbd> + <kbd>Control</kbd> + <kbd>Alt</kbd>\
8  -> ?\
9  -> ?\
10 -> ?

<a name="ligature"></a>
## Ligature ##

Is used for writing series of characters

<a name="c_rewrite"></a>
# If anyone wants to rewrite this in C #
Microsoft provides [layout samples] and [build instructions] but I
can't get them to work. So if anyone wants to try their hand at it...

[build instructions]: https://docs.microsoft.com/en-us/samples/microsoft/windows-driver-samples/keyboard-layout-samples/
[layout samples]: https://github.com/microsoft/windows-driver-samples/tree/master/input/layout "Keyboard Layout Samples"
